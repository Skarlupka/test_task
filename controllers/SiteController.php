<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegisterForm;
use app\models\Books;
use app\models\Authors;
use app\models\User;
use app\models\ProfileForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    /**
     * Displays register page.
     *
     * @return string
     */
    public function actionRegister(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new RegisterForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->actionProfile();
                }
            }
        }
        return $this->render('register', [
            'model' => $model,
        ]);
    }

    public function actionBooks(){
        $model = new Books();
        $array = $model->find()->innerjoinWith('author')->all();
        return $this->render('books', [
            'array' => $array
        ]);
    }
    public function actionAddbook($id){
        $books = new Books();
        $books->load(Yii::$app->request->post());
        $books->id_user = $id;
        $books->save();
        return $this->actionProfile();
    }
    public function actionDeletebook($id){
        Books::deleteAll(['id' => $id]);
        return $this->actionProfile();
    }
    public function actionUpdatebook($id){
        $model = Books::findOne($id);
        if($model->load(Yii::$app->request->post())){
            $model->save();
        }
        /**  $model->Text*/
        return $this->actionProfile();
    }
    public function actionAuthors(){
        $model = new Authors();
        $array = $model->find()->joinWith('books')->all();
        return $this->render('authors', [
            'array' => $array,
        ]);
    }

    public function  actionProfile(){
        $books = new Books();
        $model = new ProfileForm();
        $array = $books->find()->where(['id_user' => Yii::$app->user->identity->id])->all();
        return $this->render('profile', [
            'model' => $model,
            'array' => $array,
            'book' => $books,
        ]);
    }
    public function  actionSetfirst(){
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = 'nicker47@mail.ru';
            $user->setPassword('admin');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
    }
}
