<?php

namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

class UploadPhoto extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        if ($action->id === 'myFunction') {
            $this->enableCsrfValidation = false;
        }
        return parent::beforeAction($action);
    }

    public function actionMyFunction()
    {
        $key = Yii::$app->request->post();
        $file = $_FILES;

        $id = $key['idmodel'];
        $model = $this->findModel($id);
        $model->myFiles = UploadedFile::getInstanceByName('myFiles');
        if ($model->myUpload()) {
            $model->save(false);
        }
    }
}