<?php


namespace app\models;

use Yii;
use yii\base\Model;
use \yii\db\ActiveRecord;

class Books extends ActiveRecord
{
    public $user;
    public $title;
    public $genr;
    public $description;


    public static function tableName(){
        return 'Books';
    }

    public static function primaryKey()
    {
        return array('id');
    }
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [

            ['name', 'required'],
            ['name', 'unique', 'targetClass' => '\app\models\Books', 'message' => 'This name has already been taken.'],
            ['name', 'string', 'min' => 2, 'max' => 255],
            ['genres', 'trim'],
            ['genres', 'required'],
            ['genres', 'unique', 'targetClass' => '\app\models\Books', 'message' => 'This name has already been taken.'],
            ['genres', 'string', 'min' => 2, 'max' => 255],
            ['Text', 'trim'],
            ['Text', 'required'],
            ['Text', 'unique', 'targetClass' => '\app\models\Books', 'message' => 'This about has already been taken.'],
            ['Text', 'string', 'min' => 0, 'max' => 2550],
        ];
    }
    public function getAuthor()
    {
        return $this->hasOne(Authors::className(), ['id' => 'id_user']);
    }
}