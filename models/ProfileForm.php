<?php


namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ProfileForm is the model behind the profile form
 * @package app\models
 *
 */

class ProfileForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $verify;
    public $about;
    public $photo;



    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\ProfileForm', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\app\models\ProfileForm', 'message' => 'This email address has already been taken.'],
            ['password', 'required'],
            ['password', 'string', 'min' => 6],
            ['verify', 'required'],
            ['verify', 'compare', 'compareAttribute' => 'password'],
            ['verify', 'string', 'min' => 6],
            ['about', 'trim'],
            ['about', 'required'],
            ['about', 'unique', 'targetClass' => '\app\models\ProfileForm', 'message' => 'This about has already been taken.'],
            ['about', 'string', 'min' => 0, 'max' => 2550],
        ];
    }

}