<?php


namespace app\models;

use Yii;
use yii\base\Model;
use \yii\db\ActiveRecord;

class Authors extends ActiveRecord
{
    public static function tableName(){
        return 'Users';
    }
    public static function primaryKey()
    {
        return array('id');
    }
    public function getBooks()
    {
        return $this->hasMany(Books::className(), ['id_user' => 'id']);
    }
}