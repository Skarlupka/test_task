<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $array app\models\Authors */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Authors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="clearfix"></div>
<hr />
<table class="table table-striped table-hover">
    <tr>
        <td>#</td>
        <td>Author</td>
        <td>Photo</td>
        <td>Description</td>
        <td>Books count</td>
    </tr>
    <?php foreach ($array as $arr): ?>
        <tr>
            <td>
                <?php echo Html::a($arr->id, array('site/profile', 'id'=>$arr->id)); ?>
            </td>
            <td><?php echo Html::a($arr->username, array('site/profile', 'id'=>$arr->id)); ?></td>
            <td><?php echo Html::a(Html::img($arr->photo, ['alt' => $arr->username, 'width' => 100]), array('site/profile', 'id'=>$arr->id)); ?></td>
            <td><?php echo $arr->description; ?></td>
            <td><?php echo count($arr->books) ?></td>

        </tr>
    <?php endforeach; ?>
</table>
