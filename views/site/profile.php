<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ProfileForm */
/* @var $array app\models\ProfileForm */
/* @var $book app\models\ProfileForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\bootstrap\Modal;

$this->title = 'Profile - '.Yii::$app->user->identity->username.'';

?>
<div class="container bootstrap snippet">
    <div class="row">
        <div class="col-sm-10"><h1><?php echo Yii::$app->user->identity->username; ?></h1></div>
        <div class="col-sm-2"><?php
            echo Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Exit',
                ['class' => 'btn btn-danger logout']
            )
            . Html::endForm()
            ?></div>
    </div>
    <div class="row">
        <div class="col-sm-3"><!--left col-->

            <div class="text-center">

                <?php echo Html::img(Yii::$app->user->identity->photo, $options = ['class' => 'postImg avatar img-circle img-thumbnail', 'style' => ['alt' => Yii::$app->user->identity->username]]); ?>

                <form id="uploadPhoto">
                    <input id="myPhoto" type="file" name="myPhoto"/>
                    <input type="submit" value="Uload">
                </form>
            </div></hr><br>

        </div><!--/col-3-->
        <div class="col-sm-9">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#settings">Settings</a></li>
                <li><a data-toggle="tab" href="#books">Books</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="settings">
                    <hr>
                    <?php $form = ActiveForm::begin(['id' => 'profile-form']); ?>

                        <div class="form-group">

                            <div class="col-xs-6">
                                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'value' => Yii::$app->user->identity->username]) ?>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-xs-6">
                                <?= $form->field($model, 'email')->textInput(['value' => Yii::$app->user->identity->email]) ?>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <?= $form->field($model, 'password')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-xs-6">
                                <?= $form->field($model, 'verify')->passwordInput() ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <?= $form->field($model, 'about')->textarea(['rows' => '6']) ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <br>
                                <button class="btn btn-lg btn-success" type="submit"><i class="glyphicon glyphicon-ok-sign"></i> Update</button>
                            </div>
                        </div>

                    <?php ActiveForm::end(); ?>

                    <hr>

                </div><!--/tab-pane-->
                <div class="tab-pane" id="books">

                    <div class="clearfix"></div>
                    <hr />
                    <table class="table table-striped table-hover">
                        <label>Your books</label>
                        <tr>
                            <td>#</td>
                            <td>Title</td>
                            <td>Genres</td>
                            <td>Description</td>
                            <td></td>
                        </tr>
                        <?php foreach ($array as $arr): ?>
                            <tr>
                                <td>
                                    <?php echo Html::a($arr->id, array('site/book', 'id'=>$arr->id)); ?>
                                </td>
                                <td><?php echo Html::a($arr->name, array('site/book', 'id'=>$arr->id)); ?></td>
                                <td><?php echo Html::a($arr->genres, array('site/book', 'genre'=>$arr->genres)); ?></td>
                                <td><?php echo $arr->Text; ?></td>
                                <?php  if(!Yii::$app->user->isGuest) : ?>
                                    <td>
                                        <?php echo Html::a('Remove', array('site/deletebook', 'id'=>$arr->id), array('class'=>'btn btn-primary pull-right')); ?>
                                        <?php Modal::begin([
                                            'header' => 'Change book',
                                            'size' => 'modal-lg',
                                            'toggleButton' => [
                                            'label' => 'Change',
                                            'class' => 'btn btn-secondary pull-right'
                                        ],
                                        'footer' => Yii::$app->user->identity->username,
                                        ]);
                                        $form = ActiveForm::begin(['action' => ['site/updatebook', 'id'=>$arr->id]]);
                                        echo $form->field($book, 'name')->textInput(['autofocus' => true, 'value' => $arr->name])->label('Title');
                                        echo $form->field($book, 'genres')->textInput(['value' => $arr->genres])->label('Genres');
                                        echo $form->field($book, 'Text')->textarea(['rows' => '6', 'value' => $arr->Text])->label('Description') ;

                                        echo Html::submitButton('Change', ['class' => 'btn btn-primary', 'name' => 'changeBook-button']);

                                        ActiveForm::end();
                                        Modal::end();?>

                                    </td>
                                <?php endif; ?>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                    <?php Modal::begin([
                        'header' => '<h2>Create New Book</h2>',
                        'size' => 'modal-lg',
                        'toggleButton' => [
                                'label' => 'Create New Book',
                                'class' => 'btn btn-primary pull-right'
                        ],
                        'footer' => Yii::$app->user->identity->username,
                    ]);
                    $form = ActiveForm::begin(['action' => ['site/addbook', 'id'=>Yii::$app->user->identity->id]]);
                    echo $form->field($book, 'name')->textInput(['autofocus' => true])->label('Title');
                    echo $form->field($book, 'genres')->textInput()->label('Genres')->label('Genres');
                    echo $form->field($book, 'Text')->textarea(['rows' => '6'])->label('Description');

                    echo Html::submitButton('Create Book', ['class' => 'btn btn-primary', 'name' => 'login-button']);

                    ActiveForm::end();
                    ?>
                    <?php Modal::end(); ?>
                </div><!--/tab-pane-->
            </div><!--/tab-pane-->
        </div><!--/tab-content-->

    </div><!--/col-9-->

</div>
