<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $array app\models\Books */


use app\models\Books;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="clearfix"></div>
<hr />
<table class="table table-striped table-hover">
    <tr>
        <td>#</td>
        <td>Author</td>
        <td>Title</td>
        <td>Genres</td>
        <td>Description</td>
    </tr>
    <?php foreach ($array as $arr): ?>
        <tr>
            <td>
                <?php echo Html::a($arr->id, array('site/book', 'id'=>$arr->id)); ?>
            </td>
            <td><?php echo Html::a($arr->author->username, array('site/profile', 'id'=>$arr->id_user)); ?></td>
            <td><?php echo Html::a($arr->name, array('site/book', 'id'=>$arr->id)); ?></td>
            <td><?php echo Html::a($arr->genres, array('site/book', 'genre'=>$arr->genres)); ?></td>
            <td><?php echo $arr->Text; ?></td>
        </tr>
    <?php endforeach; ?>
</table>

